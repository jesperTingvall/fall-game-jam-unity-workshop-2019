﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIBehaviour : ScriptableObject {
    public abstract void Execute(Enemy Enemy);
    public abstract void DrawDebug(Enemy Enemy);
    public abstract void StartBehaviour(Enemy Enemy);
}
