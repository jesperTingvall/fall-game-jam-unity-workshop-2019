﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody Rigidbody;
    public float Speed;
    public float Lifetime;
    public int Damage;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
        Rigidbody.velocity = transform.forward * Speed;
        Destroy(gameObject, Lifetime);
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        var health = collision.collider.GetComponent<Health>();

        if (health != null)
        {
            health.Damage(Damage);
        }
        Destroy(gameObject);
    }

}
