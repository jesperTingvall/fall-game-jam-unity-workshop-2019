﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
    // Start is called before the first frame update

    public NavMeshAgent Agent;

    public AIBehaviour CurrentBehaviour;

    [HideInInspector]
    public GameObject CurrentTarget;

    void Start()
    {
        // Alternativ 
        // GetComponent<Health>().OnDeath.AddListener(OnDeath);
        SetBehaviour(CurrentBehaviour);
    }

    public void SetBehaviour(AIBehaviour beh)
    {
        CurrentBehaviour = beh;
        beh.StartBehaviour(this);
    }

    private void Update()
    {
        CurrentBehaviour.Execute(this);
    }

    // Update is called once per frame
    public void OnDeath()
    {
        Destroy(gameObject);
    }

    public void OnDrawGizmos()
    {
        CurrentBehaviour.DrawDebug(this);
    }
}
