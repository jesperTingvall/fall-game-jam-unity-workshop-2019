﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="AI/Find Player")]
public class FindPlayer : AIBehaviour
{
    public override void DrawDebug(Enemy Enemy)
    {
        if (Enemy.CurrentTarget) {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(Enemy.transform.position, Enemy.CurrentTarget.transform.position);
        }
    }

    public override void Execute(Enemy Enemy)
    {
        if (Enemy.CurrentTarget == null)
        {
            Enemy.CurrentTarget = GameObject.FindObjectOfType<PlayerScript>().gameObject;
        }

        Enemy.Agent.destination = Enemy.CurrentTarget.transform.position;
    }

    public override void StartBehaviour(Enemy Enemy) {
        Enemy.CurrentTarget = GameObject.FindObjectOfType<PlayerScript>().gameObject;
    }
}
