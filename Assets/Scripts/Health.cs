﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public int MaxHealth;

    [HideInInspector]
    public int CurrentHealth;

    public UnityEvent OnDeath;
    public UnityEvent OnDamage;

    void Start()
    {
        CurrentHealth = MaxHealth;
        
    }

    public void Damage(int damage)
    {
        CurrentHealth -= damage;
        OnDamage.Invoke();


        if (CurrentHealth <= 0)
        {
            OnDeath.Invoke();
        }
    }
}
