﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public CharacterController Character;

    public Transform BulletSpawnPosition;
    public GameObject BulletPrefab;

    // [HideInInspector]
    public float Speed;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        var Horizontal = Input.GetAxis("Horizontal");
        var Vertical = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(Horizontal, 0, Vertical);

        if (move.magnitude > 0)
        {
            Character.SimpleMove(move * Speed);
            transform.rotation = Quaternion.LookRotation(move, Vector3.up);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            var bullet = Instantiate(BulletPrefab, BulletSpawnPosition.position, BulletSpawnPosition.rotation);
        }

    }
}
