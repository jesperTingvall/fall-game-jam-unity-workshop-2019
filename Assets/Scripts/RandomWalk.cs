﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="AI/Random Walk")]
public class RandomWalk : AIBehaviour

{
    public float StepSize;
    public float CloseSize;

    public override void DrawDebug(Enemy Enemy) {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(Enemy.transform.position, Enemy.Agent.destination);
        Gizmos.DrawWireSphere(Enemy.transform.position, StepSize);
        Gizmos.DrawWireSphere(Enemy.Agent.destination, CloseSize);
    }

    public  override void StartBehaviour(Enemy Enemy)
    {
        var random = Random.insideUnitCircle;
        Enemy.Agent.destination = Enemy.transform.position + new Vector3(random.x, 0, random.y) * StepSize;
    }

    public override void Execute(Enemy Enemy) {
        Debug.Log("WALKING!");
        var distance = Enemy.transform.position - Enemy.Agent.destination;

        if (distance.magnitude < CloseSize) {
            var random = Random.insideUnitCircle;
            Enemy.Agent.destination = Enemy.transform.position + new Vector3(random.x, 0, random.y) * StepSize;
        }
        
    }
}
